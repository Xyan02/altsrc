# README #

Coding Challenge for AltSource created by Xianguang Yan
Point of Contact: xianguang.yan@gmail.com

### What is this repository for? ###

Given this API: https://api.punkapi.com/v2/beers
- Create a List that contains all the Beers
- When Clicked Show Next Beer Screen
- Must Contain Name, Tagline, Description of the beer in a nicely formatted way

### Basic Requirements: ###
- Creating Layout/UIs using standard UI elements provided by the platform.
	See Pictures
- Makign a network call either using standard classes or using a 3rd party library.
	Here I used Android Volley which is an HTTP library that makes networkign for Android apps easier and more importantly faster.
	Allows for Automatic Scheduling and Asynchronous calls so it doesn't bottleneck the UI thread.
- Parsing JSON using some JSON parsing library
	Used the default JSONObjects to parse the String
- Implement at least MVC pattern.
	Done.
	The Activities are the Views in MVC. Controllers are a seperate class that don't extend or use any Android Class.
	This makes it straitforward to unit test. The Controller doesn't extend or implment any Android Classes and should
	have a reference to an interface class of the View.
	This makes Unit Testing of Controller also possible.
- Suubmit the project using Github or Bitbucket as a public repository
	Done

### Extra Credit: ###
- Additional Features added is Unit Test for BeerItem
- Tested functionality of BeerTest with JUNIT
- Used Espresso To do UI Tests to allow for automated testing
- Tested the interactions of RecyclerView and the contents inside to assert that they were there
- Also added additional features like Pictures of beers using Picasso
- Also integrated internal storing to speed up the process of loading the MainActivity

### Libraries Used: ###
- Espresso
- AndroidJunitRunner and JUnit Rules
- ButterKnife
- RxJava2
- Volley
- RecyclerView
- Picasso

![image](example1.jpg) = 540x860

![image](example2.jpg) = 540x860
