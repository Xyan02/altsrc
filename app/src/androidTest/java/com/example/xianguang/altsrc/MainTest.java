package com.example.xianguang.altsrc;


import android.app.Activity;
import android.support.test.espresso.action.ViewActions;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;

import com.example.xianguang.altsrc.MainActivity;
import com.example.xianguang.altsrc.R;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;


/**
 * No Longer In Use to Test ReyclerView
 */
public class MainTest {

    @RunWith(AndroidJUnit4.class)
    @LargeTest
    public class ChangeTextBehaviorTest {

        public static final String STRING_TO_BE_TYPED = "Espresso";
        public static final int ITEM_BELOW_THE_FOLD = 0;

        @Rule
        public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(
                MainActivity.class);

        /*
        @Test
        public void changeText_sameActivity() {
            // Type text and then press the button.
            onView(withId(R.id.editTextUserInput))
                    .perform(typeText(STRING_TO_BE_TYPED), closeSoftKeyboard());
            onView(withId(R.id.changeTextBt)).perform(click());

            // Check that the text was changed.
            onView(withId(R.id.textToBeChanged)).check(matches(withText(STRING_TO_BE_TYPED)));
        }

        @Test
        public void changeText_newActivity() {
            // Type text and then press the button.
            onView(withId(R.id.editTextUserInput)).perform(typeText(STRING_TO_BE_TYPED),
                    closeSoftKeyboard());
            onView(withId(R.id.activityChangeTextBtn)).perform(click());

            // This view is in a different Activity, no need to tell Espresso.
            onView(withId(R.id.show_text_view)).check(matches(withText(STRING_TO_BE_TYPED)));
        }
        */

        @Test
        public void scrollToItemBelowFold_checkItsText() {
            // First, scroll to the position that needs to be matched and click on it.
            onView(ViewMatchers.withId(R.id.recyclerView))
                    .perform(RecyclerViewActions.actionOnItemAtPosition(0,
                            click()));

            // Match the text in an item below the fold and check that it's displayed.
            String itemElementText = mActivityRule.getActivity().getResources()
                    .getString(R.string.element_text)
                    + String.valueOf(0);
            onView(withText(itemElementText)).check(matches(isDisplayed()));
        }

    }
}