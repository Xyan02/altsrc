package com.example.xianguang.altsrc;

import java.io.Serializable;
import java.util.List;

public class Beers implements Serializable{
    private List<BeerItem> beerItemList;

    public Beers(List<BeerItem> beerItemList) {
        this.beerItemList = beerItemList;
    }

    public List<BeerItem> getBeerItemList() {
        return beerItemList;
    }
}
