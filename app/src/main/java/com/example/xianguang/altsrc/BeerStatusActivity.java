package com.example.xianguang.altsrc;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BeerStatusActivity extends AppCompatActivity {

    private BeerItem beerItem;

    @BindView(R.id.toolbar_top)
    Toolbar toolbar;


    @BindView(R.id.beer_name)
    TextView beerNameTextView;
    @BindView(R.id.beer_description)
    TextView beerDescriptionTextView;
    @BindView(R.id.beer_image)
    ImageView beerImageView;
    @BindView(R.id.beer_tagline)
    TextView beerTaglineTextView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.beer_status_activity);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        beerItem = (BeerItem) getIntent().getSerializableExtra(MainActivity.BEER_DATA);

        //Load Everything
        if (beerItem != null) {
            getSupportActionBar().setTitle(beerItem.getName());
            centerToolBar();
            beerDescriptionTextView.setText(beerItem.getDescription());
            beerNameTextView.setText(beerItem.getName());
            beerTaglineTextView.setText(beerItem.getTagline());
            Picasso.get().load(beerItem.getImage_url()).into(beerImageView);
        }
    }

    private void centerToolBar() {
        for(int i = 0; i < toolbar.getChildCount(); i++) {
            View child = toolbar.getChildAt(i);
            if(child instanceof TextView && ((TextView) child).getText().equals(beerItem.getName())) {
                TextView toolbarTitle = (TextView) child;
                toolbarTitle.setTextColor(Color.BLACK);
            }
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
