package com.example.xianguang.altsrc;

import java.io.Serializable;

public class BeerItem implements Serializable {
    private int id;
    private String name;
    private String tagline;
    private String description;
    private String image_url;

    public BeerItem (int id, String name, String tagline, String description, String image_url){
        this.name = name;
        this.tagline = tagline;
        this.description = description;
        this.image_url = image_url;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getTagline() {
        return tagline;
    }

    public String getDescription() {
        return description;
    }

    public String getImage_url() {
        return image_url;
    }
}
