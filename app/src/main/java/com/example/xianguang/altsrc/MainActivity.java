package com.example.xianguang.altsrc;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.xianguang.altsrc.Adapters.BeerAdapter;
import com.example.xianguang.altsrc.Adapters.BeerItemDecoration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements BeerAdapter.ItemClickListener {
    public static final String URL_DATA = "https://api.punkapi.com/v2/beers";
    public static final String BEER_DATA = "BEEROBJECT";
    public static final String FILE_NAME = "Beers";

    @BindView(R.id.toolbar_top)
    Toolbar toolbar;
    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;

    private BeerAdapter beerAdapter;
    private Beers myBeer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Beers");
        centerToolBar();

        try {
            FileInputStream fis = getApplicationContext().openFileInput(FILE_NAME);
            ObjectInputStream is = new ObjectInputStream(fis);
            myBeer = (Beers) is.readObject();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        if(myBeer == null) {
            //Initialize Beer Text
            myBeer = new Beers(new ArrayList<BeerItem>());
            loadUrlData();
        }



        //Initialize Adapter + RecyclerView
        beerAdapter = new BeerAdapter(myBeer.getBeerItemList());
        beerAdapter.setOnClickListener(this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(beerAdapter);

        //Item Decor
        BeerItemDecoration dividerDecor = new BeerItemDecoration(getResources().getDrawable(R.drawable.divider));
        mRecyclerView.addItemDecoration(dividerDecor);

    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            FileOutputStream fos = getApplicationContext().openFileOutput(FILE_NAME, Context.MODE_PRIVATE);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(myBeer);
            os.close();
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void centerToolBar() {
        for (int i = 0; i < toolbar.getChildCount(); i++) {
            View child = toolbar.getChildAt(i);
            if (child instanceof TextView) {
                TextView toolbarTitle = (TextView) child;
                Toolbar.LayoutParams params = new Toolbar.LayoutParams(Toolbar.LayoutParams.WRAP_CONTENT, Toolbar.LayoutParams.WRAP_CONTENT);
                params.gravity = Gravity.CENTER_HORIZONTAL;
                toolbarTitle.setLayoutParams(params);
                toolbarTitle.setTextColor(Color.BLACK);

            }
        }
    }

    private void loadUrlData() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_DATA, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();

                try {

                    JSONArray array = new JSONArray(response);

                    for (int i = 0; i < array.length(); i++) {

                        JSONObject jo = array.getJSONObject(i);

                        BeerItem developers = new BeerItem(jo.getInt("id"), jo.getString("name"), jo.getString("tagline"),
                                jo.getString("description"), jo.getString("image_url"));
                        Log.d("BeerName", "BEERNAME: " + jo.getString("name"));
                        myBeer.getBeerItemList().add(developers);

                    }

                    beerAdapter.notifyDataSetChanged();

                } catch (JSONException e) {

                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(MainActivity.this, "Error" + error.toString(), Toast.LENGTH_SHORT).show();

            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    @Override
    public void onItemClick(View view, int position) {
        //Toast.makeText(this, "You clicked " + beerAdapter.getItem(position).getName() + " on row number " + position, Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(view.getContext(), BeerStatusActivity.class);
        intent.putExtra("BEEROBJECT", myBeer.getBeerItemList().get(position));
        startActivity(intent);

    }

}
