package com.example.xianguang.altsrc;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class UnitTestBeerItem {
    @Test
    public void testBeerItem() {
        int id = 0;
        String name = "Beer Name";
        String tag = "Tag";
        String description = "Description";
        String url = "ImageURL";

        BeerItem testBeer = new BeerItem(id, name, tag, description, url);
        assertEquals(testBeer.getName(), name);
        assertEquals(testBeer.getTagline(), tag);
        assertEquals(testBeer.getDescription(), description);
        assertEquals(testBeer.getImage_url(), url);
    }
}
